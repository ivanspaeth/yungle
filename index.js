/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const {app, BrowserWindow} = require("electron");
const path = require("path");
const url = require("url");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * PRIVATE PROPERTIES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Keep a global reference of the window object, if you don"t, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * PRIVATE METHODS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Creates the main window.  Can be called multiple times.  Will
 * only open if not already active.   Subsequent calls will have
 * no effect.
 */
const createWindow = () => {
    // make sure we don't have a window already
    if (!win) {
        // Create the browser window.
        win = new BrowserWindow({
            center: true,
            // frame: false,
            show: false
        });
        // remove menu
        win.setMenu(null);
        // Maximize the window
        win.maximize();
        // open the dev tools
        // win.webContents.openDevTools();
        // and load the index.html of the app.
        win.loadURL(url.format({
            pathname: path.join(__dirname, "index.html"),
            protocol: "file:",
            slashes: true
        }));
        // Emitted when the window is ready to be shown
        win.once("ready-to-show", () => {
            // show the window
            win.show();
        });
        // Emitted when the window is closed.
        win.on("closed", () => {
            // Dereference the window object, usually you would store windows
            // in an array if your app supports multi windows, this is the time
            // when you should delete the corresponding element.
            win = null;
        });
    }
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EVENTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
// On macOS it is common for applications and their menu bar
// to stay active until the user quits explicitly with Cmd + Q
app.on("window-all-closed", () => {
    // close everything
    app.quit();
});

// On macOS it"s common to re-create a window in the app when the
// dock icon is clicked and there are no other windows open.
app.on("activate", createWindow);