/*jslint browser: true*/

(function ($angular) {

    "use strict";

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Module Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $angular
        .module("yungle", [
            "ngAnimate",
            "ngRoute",
            "app/AppController",
            "dashboard/DashboardController",
            "navigation/NavigationController",
            "project/ProjectController",
            "settings/SettingsController"
        ])
        .config([
            "$routeProvider", function ($routeProvider) {
                $routeProvider.otherwise({
                    redirectTo: "/dashboard"
                });
            }
        ]);

}(window.angular));
/*jslint browser: true*/

(function ($angular) {

    "use strict";

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Controller Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    var controller = function ($rootScope, $scope) {
        $rootScope.sidebar = false;
    };

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Module Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $angular
        .module("settings/SettingsController", [
            "settings/SettingsView"
        ])
        .controller("settings/SettingsController", [
            "$rootScope",
            "$scope",
            controller
        ])
        .config([
            "$routeProvider", function ($routeProvider) {
                $routeProvider.when("/settings", {
                    templateUrl: "settings/SettingsView",
                    controller: "settings/SettingsController"
                });
            }
        ]);

}(window.angular));
/*global angular: false */
angular.module("settings/SettingsView", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("settings/SettingsView",
    "<h1>Settings</h1><a href=#!/dashboard>Dashboard</a>");
}]);

/*jslint browser: true*/

(function ($angular) {

    "use strict";

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Controller Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    var controller = function ($scope) {
        
    };

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Module Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $angular
        .module("project/ProjectController", [
            "project/ProjectView"
        ])
        .controller("project/ProjectController", [
            "$scope",
            controller
        ]);

}(window.angular));
/*global angular: false */
angular.module("project/ProjectView", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("project/ProjectView",
    "<div id=window-project ng-controller=project/ProjectController><ul><li class=project><i class=\"far fa-folder-open\"></i> Project<ul><li class=directory><a href=#!/dashboard><i class=\"far fa-folder\"></i> <span>node_modules</span></a></li><li class=directory><a href=#!/dashboard><i class=\"far fa-folder\"></i> <span>public</span></a></li><li class=\"file extension-gitattributes\"><a href=#!/dashboard><i class=\"far fa-file\"></i> <span>.gitattributes</span></a></li><li class=\"file extension-gitignore\"><a href=#!/dashboard><i class=\"far fa-file\"></i> <span>.gitignore</span></a></li><li class=\"file extension-js\"><a href=#!/dashboard><i class=\"far fa-file\"></i> <span>Gruntfile.js</span></a></li><li class=\"file extension-html\"><a href=#!/dashboard><i class=\"far fa-file\"></i> <span>index.html</span></a></li><li class=\"file extension-js\"><a href=#!/dashboard><i class=\"far fa-file\"></i> <span>index.js</span></a></li><li class=file><a href=#!/dashboard><i class=\"far fa-file\"></i> <span>LICENSE</span></a></li><li class=\"file extension-json\"><a href=#!/dashboard><i class=\"far fa-file\"></i> <span>package.json</span></a></li><li class=\"file extension-json\"><a href=#!/dashboard><i class=\"far fa-file\"></i> <span>package-lock.json</span></a></li><li class=\"file extension-md\"><a href=#!/dashboard><i class=\"far fa-file\"></i> <span>README.md</span></a></li></ul></li></ul></div>");
}]);

/*jslint browser: true*/

(function ($angular) {

    "use strict";

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Controller Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    var controller = function ($scope) {
        $scope.navigation = "dashboard";
    };

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Module Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $angular
        .module("navigation/NavigationController", [
            "navigation/NavigationView"
        ])
        .controller("navigation/NavigationController", [
            "$scope",
            controller
        ])
        .config([
            "$routeProvider", function ($routeProvider) {
                $routeProvider.when("/navigation", {
                    templateUrl: "navigation/NavigationView",
                    controller: "navigation/NavigationController"
                });
            }
        ]);

}(window.angular));
/*global angular: false */
angular.module("navigation/NavigationView", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("navigation/NavigationView",
    "<ul id=window-navigation ng-controller=navigation/NavigationController><li ng-class=\"{ 'active': navigation === 'dashboard' }\"><a href=#!/dashboard ng-click=\"navigation = 'dashboard'\"><i class=\"fa fa-file\"></i></a></li><li ng-class=\"{ 'active': navigation === 'settings' }\"><a href=#!/settings ng-click=\"navigation = 'settings'\"><i class=\"fa fa-cogs\"></i></a></li><li ng-class=\"{ 'active': navigation === 'help' }\"><a href=#!/help ng-click=\"navigation = 'help'\"><i class=\"fa fa-info\"></i></a></li></ul>");
}]);

/*jslint browser: true*/

(function ($angular) {

    "use strict";

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Controller Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    var controller = function ($rootScope, $scope) {
        $rootScope.sidebar = true;
    };

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Module Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $angular
        .module("dashboard/DashboardController", [
            "dashboard/DashboardView"
        ])
        .controller("dashboard/DashboardController", [
            "$rootScope",
            "$scope",
            controller
        ])
        .config([
            "$routeProvider", function ($routeProvider) {
                $routeProvider.when("/dashboard", {
                    templateUrl: "dashboard/DashboardView",
                    controller: "dashboard/DashboardController"
                });
            }
        ]);

}(window.angular));
/*global angular: false */
angular.module("dashboard/DashboardView", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("dashboard/DashboardView",
    "<div ng-include=\"'project/ProjectView'\"></div><h1>Dashboard</h1><a href=#!/settings>Settings</a>");
}]);

/*jslint browser: true*/

(function ($angular) {

    "use strict";

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Controller Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    var controller = function ($scope) {
        $scope.sidebar = false;
    };

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Module Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $angular
        .module("app/AppController", [])
        .controller("app/AppController", [
            "$rootScope",
            controller
        ]);

}(window.angular));