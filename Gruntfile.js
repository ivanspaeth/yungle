/*jslint node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORT VARIABLES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const path = require("path");
const walk = require("walk");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * HELPER FUNCTIONS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Escape function for creating valid name space names.
 * @param {String} $string the string to escape
 * @return {String}
 */
const escapeRegExp = ($string) => $string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");

/**
 * Helper function for finding and replacing all occurrences of the string
 * @param {String} $string the string to search
 * @param {String} $find the string to find in the search parameter
 * @param {String} $replace the string to replace the find parameter with
 * @return {String}
 */
const replaceAll = ($string, $find, $replace) => $string.replace(new RegExp(escapeRegExp($find), "g"), $replace);

/**
 * Performs a recursive search for files that have the specified extension, starting with the base path
 * @param {String} $basePath the path to start searching from
 * @param {String} $extension the file extension to find
 * @return {String[]}
 */
const get_files = ($basePath, $extension) => {
    const files = [];
    const options = {
        listeners: {
            names: function (root, nodeNamesArray) {
                nodeNamesArray.sort(function (a, b) {
                    if (a > b) {
                        return 1;
                    }
                    if (a < b) {
                        return -1;
                    }
                    return 0;
                });
            },
            directories: function (root, dirStatsArray, next) {
                next();
            },
            file: function (root, fileStats) {
                const name = fileStats.name;
                if (name.split("").reverse().join("").substring(0, $extension.length + 1) === $extension.split("").reverse().join("") + ".") {
                    files.push(path.join(root, name));
                }
            },
            errors: function (root, nodeStatsArray, next) {
                next();
            }
        }
    };
    walk.walkSync($basePath, options);
    return files;
};

/**
 * Helper function to return the options to pass to the HTML2JS grunt task
 * @return {{}}
 */
const get_html2js = () => {
    let i;
    let t;
    const files = get_files(path.resolve("source/js"), "html");
    const options = {
        options: {
            module: false,
            base: "",
            useStrict: true,
            fileHeaderString: "/*global angular: false */",
            htmlmin: {
                collapseBooleanAttributes: true,
                collapseWhitespace: true,
                removeAttributeQuotes: true,
                removeComments: true,
                removeEmptyAttributes: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true
            },
            rename: function(moduleName) {
                return moduleName.replace(".html", "").replace("source/js/", "");
            }
        }
    };
    for (i = 0, t = files.length; i < t; i = i + 1) {
        options[replaceAll(replaceAll(files[i].substring(path.resolve("source/js").length + 1, files[i].length), ":", "-"), "\\", "/")] = {
            src: [files[i]],
            dest: files[i].substr(0, files[i].lastIndexOf(".")) + ".js"
        }
    }
    return options;
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * The main grunt function to be called when the grunt command is run
 * @param {Object} grunt a reference to the grunt library
 * @return {void}
 */
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        concat_css: {
            options: {
                assetBaseUrl: 'public',
                baseDir: 'source/(css|webfonts)'
            },
            lib: {
                src: [
                    "node_modules/@fortawesome/fontawesome-free-webfonts/css/fa-brands.css",
                    "node_modules/@fortawesome/fontawesome-free-webfonts/css/fa-regular.css",
                    "node_modules/@fortawesome/fontawesome-free-webfonts/css/fa-solid.css",
                    "node_modules/@fortawesome/fontawesome-free-webfonts/css/fontawesome.css"
                ],
                dest: "public/css/lib.css"
            },
            app: {
                src: [
                    "source/css/reset.css",
                    "source/css/base.css",
                    "source/css/extensions.css"
                ],
                dest: "public/css/app.css"
            }
        },
        cssmin: {
            options: {
                sourceMap: false,
                assetBaseUrl: "",
                baseDir: ""
            },
            app: {
                files: {
                    "public/css/app.min.css": [
                        "public/css/app.css"
                    ]
                }
            },
            lib: {
                files: {
                    "public/css/lib.min.css": [
                        "public/css/lib.css"
                    ]
                }
            }
        },
        html2js: get_html2js(),
        concat: {
            lib: {
                src: [
                    "node_modules/angular/angular.js",
                    "node_modules/angular-animate/angular-animate.js",
                    "node_modules/angular-route/angular-route.js"
                ],
                dest: "public/js/lib.js"
            },
            app: {
                src: get_files(path.resolve("source/js"), "js"),
                dest: "public/js/app.js"
            }
        },
        imagemin: {
            dynamic: {
                files: [
                    {
                        expand: true,
                        cwd: "public/img/",
                        src: ["**/*.{png,jpg,gif}"],
                        dest: "public/img/"
                    }
                ]
            }
        },
        uglify: {
            options: {
                sourceMap: true
            },
            app: {
                files: {
                    "public/js/app.min.js": "public/js/app.js"
                }
            },
            lib: {
                files: {
                    "public/js/lib.min.js": "public/js/lib.js"
                }
            }
        },
        copy: {
            main: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        flatten: true,
                        src: ['node_modules/@fortawesome/fontawesome-free-webfonts/webfonts/*'],
                        dest: 'public/css/webfonts/',
                        filter: 'isFile'
                    }
                ],
            },
        }
    });

    // make sure we only process files that were altered
    grunt.loadNpmTasks("grunt-newer");
    // Load the plugin that provides the "css concatenation" task.
    grunt.loadNpmTasks("grunt-concat-css");
    // Load the plugin that provides the css concatenation
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    // Load the plugin that converts the templates to js
    grunt.loadNpmTasks("grunt-html2js");
    // Load the plugin that provides the "js concatenation" task.
    grunt.loadNpmTasks("grunt-contrib-concat");
    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks("grunt-contrib-uglify");
    // Load the plugin that provides the "copy" task.
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Default task(s).
    grunt.registerTask("build-css", ["concat_css", "cssmin", "copy"]);
    grunt.registerTask("build-js", ["html2js", "concat", "uglify"]);
    grunt.registerTask("build-app", ["concat_css:app", "cssmin:app", "html2js", "concat:app", "uglify:app"]);
    grunt.registerTask("build-lib", ["concat_css:lib", "cssmin:lib", "concat:lib", "uglify:lib", "copy"]);
    grunt.registerTask("build-img", ["newer:imagemin"]);

};
