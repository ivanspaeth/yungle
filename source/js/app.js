/*jslint browser: true*/

(function ($angular) {

    "use strict";

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Module Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $angular
        .module("yungle", [
            "ngAnimate",
            "ngRoute",
            "app/AppController",
            "dashboard/DashboardController",
            "navigation/NavigationController",
            "project/ProjectController",
            "settings/SettingsController"
        ])
        .config([
            "$routeProvider", function ($routeProvider) {
                $routeProvider.otherwise({
                    redirectTo: "/dashboard"
                });
            }
        ]);

}(window.angular));