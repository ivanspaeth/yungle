/*jslint browser: true*/

(function ($angular) {

    "use strict";

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Controller Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    var controller = function ($rootScope, $scope) {
        $rootScope.sidebar = false;
    };

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Module Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $angular
        .module("settings/SettingsController", [
            "settings/SettingsView"
        ])
        .controller("settings/SettingsController", [
            "$rootScope",
            "$scope",
            controller
        ])
        .config([
            "$routeProvider", function ($routeProvider) {
                $routeProvider.when("/settings", {
                    templateUrl: "settings/SettingsView",
                    controller: "settings/SettingsController"
                });
            }
        ]);

}(window.angular));