/*jslint browser: true*/

(function ($angular) {

    "use strict";

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Controller Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    var controller = function ($scope) {
        $scope.navigation = "dashboard";
    };

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Module Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $angular
        .module("navigation/NavigationController", [
            "navigation/NavigationView"
        ])
        .controller("navigation/NavigationController", [
            "$scope",
            controller
        ])
        .config([
            "$routeProvider", function ($routeProvider) {
                $routeProvider.when("/navigation", {
                    templateUrl: "navigation/NavigationView",
                    controller: "navigation/NavigationController"
                });
            }
        ]);

}(window.angular));