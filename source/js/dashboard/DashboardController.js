/*jslint browser: true*/

(function ($angular) {

    "use strict";

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Controller Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    var controller = function ($rootScope, $scope) {
        $rootScope.sidebar = true;
    };

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Module Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $angular
        .module("dashboard/DashboardController", [
            "dashboard/DashboardView"
        ])
        .controller("dashboard/DashboardController", [
            "$rootScope",
            "$scope",
            controller
        ])
        .config([
            "$routeProvider", function ($routeProvider) {
                $routeProvider.when("/dashboard", {
                    templateUrl: "dashboard/DashboardView",
                    controller: "dashboard/DashboardController"
                });
            }
        ]);

}(window.angular));