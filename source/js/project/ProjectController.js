/*jslint browser: true*/

(function ($angular) {

    "use strict";

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Controller Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    var controller = function ($scope) {
        
    };

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Module Definition
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $angular
        .module("project/ProjectController", [
            "project/ProjectView"
        ])
        .controller("project/ProjectController", [
            "$scope",
            controller
        ]);

}(window.angular));